package com.algaworks.brewer.config.init;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.algaworks.brewer.config.JPAConfig;
import com.algaworks.brewer.config.ServiceConfig;
import com.algaworks.brewer.config.WebConfig;

/**
 * A classe {@code AppInitializer } serve para mapear URL, fazendo papel do
 * FrontController.
 * 
 * @author Filipe Lobato
 */
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	/**
	 * Configurações do serviço pra trás
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { JPAConfig.class, ServiceConfig.class };
	}

	/**
	 * Configurações do WEB - Controller pra frente
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		// Retorna classe que ensina o spring a achar os Controllers
		return new Class<?>[] { WebConfig.class };
	}

	/**
	 * Dispatcher Servlet Este método diz o padrão da URL que será delegado para
	 * o DispatcherServlet (FrontController)
	 */
	@Override
	protected String[] getServletMappings() {
		// Qualquer URL a partir de / será enviado ao DispatcherServlet
		return new String[] { "/" };
	}

	// Forçar os arquivos a serem UTF8
	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);

		return new Filter[] { characterEncodingFilter };
	}

	@Override
	protected void customizeRegistration(Dynamic registration) {
		// Seta local onde o arquivo temporário será salvo no servidor, 
		// "" é por conta do servidor
		registration.setMultipartConfig(new MultipartConfigElement(""));
	}
}
