# brewer

![node](https://img.shields.io/badge/Java-1.8.0-lightgray.svg) 
![node](https://img.shields.io/badge/Thymeleaf-3.0.6.RELEASE-yellowgreen.svg)
![node](https://img.shields.io/badge/Thymeleaf%20Layout--Dialect-2.0.0-yellow.svg)
![node](https://img.shields.io/badge/Hibernate-5.4.3.Final-red.svg)
![node](https://img.shields.io/badge/Logging-2.6-blue.svg)
![node](https://img.shields.io/badge/Servlet%20API-3.1.0-yellowgreen.svg)

## Configurações básicas
	Ao rodar o projeto, caso haja problemas com acentuação, basta configurar o seu Eclipse em Windows > Preferences > Workspace:
		Text file encoding = UTF-8
		New text file line delimiter = Unix
	Após isso, basta resalvar as páginas que apresentam o erro.

### Ferramentas utilizadas:
	Para upload de fotos: 	<a href="https://getuikit.com/docs/upload" rel="nofollow">UIKIT</a>
	Para criação de templates dinamicamentes, esta bibliteca JS: https://handlebarsjs.com/ (Você cria um template como JS, depois passa os objetos e ela compila o html)
	API pararedimensionar fotos: https://github.com/coobird/thumbnailator
	
	